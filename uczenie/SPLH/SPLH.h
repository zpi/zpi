#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <ostream>
#include <vector>


class SPLH
{
	struct Hasher
	{
		Eigen::MatrixXd W;
		double b;
	};

public:

	struct InputData
	{
		std::string label;
		std::vector<double> data;
	};

	void SaveModelCsv(std::ostream &output, char separator = '\t') const;

	void Solve(int K, double alpha);

	void Load(int Width, int Height, std::vector<InputData> images);

private:

	Eigen::MatrixXd bestEighenVector(Eigen::MatrixXd &M);

	double dataMean(Eigen::MatrixXd &W);

	Eigen::MatrixXd T(Eigen::MatrixXd &Sk, Eigen::MatrixXd &Xk, Eigen::MatrixXd &W);

private:

	int Width, Height;			// wymiar obrazka
	int D;						// wybiar danych
	int N;						// liczba obrazk�w;

	std::vector<Hasher> solution;

	Eigen::MatrixXd S;
	Eigen::MatrixXd X;
};