#include "SPLH.h"

void SPLH::SaveModelCsv(std::ostream &output, char separator) const
{
	output << solution.size() << separator << D << std::endl;
	for (const Hasher &h : solution)
	{
		output << h.b;
		for (int i = 0; i < D; ++i)
			output << separator << h.W(i);
		output << std::endl;
	}
}

void SPLH::Solve(int K, double alpha)
{
	solution.clear();

	Eigen::MatrixXd Sk = S;
	Eigen::MatrixXd Xk = X;

	for (int k = 0; k < K; ++k)
	{
		Eigen::MatrixXd M = Xk * Sk * Xk.transpose();

		Eigen::MatrixXd W = bestEighenVector(M);
		double b = dataMean(W);

		solution.push_back({ W, b });

		Sk -= alpha * T(Sk, Xk, W);

		Xk -= W*W.transpose()*Xk;
	}

}

void SPLH::Load(int Width, int Height, std::vector<InputData> images)
{
	this->Width = Width;
	this->Height = Height;
	D = Width * Height * 3;
	N = images.size();

	X = Eigen::MatrixXd::Zero(D, N);
	S = Eigen::MatrixXd::Zero(N, N);

	for (int j = 0; j < N; ++j)
		for (int i = 0; i < D; ++i)
			X(i, j) = images[j].data[i];

	for (int j = 0; j < N; ++j)
		for (int i = 0; i < N; ++i)
			S(i, j) = images[j].label == images[i].label ? 1 : -1;
}

Eigen::MatrixXd SPLH::bestEighenVector(Eigen::MatrixXd &M)
{
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es(M);

	auto alpha = es.eigenvalues();
	auto v = es.eigenvectors();

	int bestAlpha = 0;
	for (int i = 1; i < alpha.rows(); ++i)
		if (alpha(i, 0) > alpha(bestAlpha, 0))
			bestAlpha = i;

	return v.col(bestAlpha);
}

double SPLH::dataMean(Eigen::MatrixXd &W)
{
	double b = 0;
	for (int j = 0; j < X.cols(); ++j)
		b -= (W.transpose() * X.col(j))(0, 0);
	b /= X.cols();

	return b;
}

Eigen::MatrixXd SPLH::T(Eigen::MatrixXd &Sk, Eigen::MatrixXd &Xk, Eigen::MatrixXd &W)
{
	Eigen::MatrixXd Stk = Xk.transpose()*W*W.transpose()*Xk;;

	for (int n = 0; n < Stk.rows(); ++n)
		for (int m = 0; m < Stk.cols(); ++m)
			if (S(n, m) * Stk(n, m) >= 0)
				Stk(n, m) = 0;

	return Stk;
}
	