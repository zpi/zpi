#include <iostream>
#include <ctime>
#include <fstream>
#include "SPLH.h"

int main(int argc, char **argv)
{
	SPLH solver;

	int width;
	int height;
	double alpha;
	int K;

	std::vector<SPLH::InputData> images;

	solver.Load(width, height, images);

	solver.Solve(K, alpha);

	std::string outputCsv;

	{
		std::ofstream csv(outputCsv);
		solver.SaveModelCsv(csv);
	}
	return 0;
}