package server;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class MatrixServerConnection {

	private static final String SERVICE_URL = "http://www.maranatha-wackernheim.de/services/zpitest.php";
	private Activity parentActivity;
	
	
	public MatrixServerConnection(Activity parentActivity){
		this.parentActivity = parentActivity;
	}
	
	public void connectToServer() {
		if(isOnline())
			getMatrixFromServer();
		else 
			Toast.makeText(parentActivity, "You are offline. This app needs internet connection to work.", Toast.LENGTH_LONG).show();
	}

	private void getMatrixFromServer() {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(SERVICE_URL);
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		try {
			String response = client.execute(post, responseHandler);
			handleServerMatrixResponse(response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void handleServerMatrixResponse(String response) {
		JSONObject jsonObject;
		String decodedResponse = "";
		try {
			jsonObject = new JSONObject(response);
			decodedResponse = jsonObject.getString("matrix");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Toast.makeText(parentActivity, decodedResponse, Toast.LENGTH_LONG).show();
	}

	private boolean isOnline() {
		ConnectivityManager connMgr = (ConnectivityManager)parentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		return networkInfo != null && 
			   networkInfo.isConnected();
	}
	
}
