package pl.zpi.gi;

import pl.zpi.gi.R;
import server.MatrixServerConnection;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class GIActivity extends Activity{
	private Context mContext = this;
	
	private static final int REQUEST_IMAGE_CAPTURE = 1;
	
	private ImageView mImageView;
	private Button mButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gi);
		
		setComponents();
		
		runUpdater();
		
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_create));
	}
	
	private void setComponents(){
		mImageView = (ImageView) findViewById(R.id.capture_img);
		
		mButton = (Button) findViewById(R.id.camera_btn);
		mButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent();
			}
		});
	}
	
	private void runUpdater(){
		MatrixServerConnection msc = new MatrixServerConnection(this);
		msc.connectToServer();
	}

	/** Intent to CameraActivity */
	private void dispatchTakePictureIntent() {
		Intent intent = new Intent(this, CameraActivity.class);
		startActivity(intent);// TODO 1 - resultActivity
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		if(checkCameraHardware(mContext)){
			Log.i(getString(R.string.tag_gi_activity), getString(R.string.log_camera_exist) + true);
		} else{
			;//TODO toast "no camera"
			Log.e(getString(R.string.tag_gi_activity), getString(R.string.log_camera_exist) + false);
		}
			
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_start));
	}
	
	/** Check if this device has a camera */
	private boolean checkCameraHardware(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        // this device has a camera
	        return true;
	    } else {
	        // no camera on this device
	        return false;
	    }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {//TODO 2
	    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
	        Bundle extras = data.getExtras();
	        Bitmap imageBitmap = (Bitmap) extras.get("data");
	        mImageView.setImageBitmap(imageBitmap);
	    }
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_destroy));
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_pause));
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_restart));
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_resume));
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(getString(R.string.tag_gi_activity), getString(R.string.log_on_stop));
	}
}
